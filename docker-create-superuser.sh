#!/usr/bin/env bash

docker run -v $(pwd)/hackerone/:/django/hackerone/ -p 8000:8000 -ti hackerone-shaneengelman \
    python hackerone/manage.py migrate && python hackerone/manage.py createsuperuser
