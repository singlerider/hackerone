FROM python:3.7.4-alpine3.10
ENV LANG C.UTF-8

RUN mkdir /django

RUN apk update

RUN apk add py-gunicorn

RUN pip install --upgrade pip

COPY requirements.txt /django/requirements.txt
RUN pip install -r /django/requirements.txt

COPY . /django

WORKDIR /django

EXPOSE 8000

CMD ["sh", "/django/run.sh"]
