#!/usr/bin/env bash

python hackerone/manage.py migrate
python hackerone/manage.py makemigrations report
python hackerone/manage.py migrate
python hackerone/manage.py runsslserver 0.0.0.0:8000
# docker-compose run server ./manage.py createsuperuser
