# Hackerone Code Challenge

REST API for Reports.

## Setup

### The "Easy" Way (Docker)

1.  Build the Docker image (based on python:3.7.4-alpine3.10):

```shell
$ ./docker-build.sh
```

2.  Create an admin `User` with password. Repeat as many times as you'd like:

```shell
$ ./docker-create-superuser.sh
```

-   Create a username
-   Add an email address
-   Create a password
    -   Type the password a second time

## Run

### The "Easy" Way (Docker)

```shell
$ ./docker-run.sh
```

## Interact

### Web View (Recommended)

Head to <https://0.0.0.0:8000/>. Accept the unverified TLS certificate [for the purposes of a code challenge] and log in with the credentials created during setup.

### cURL

#### OPTIONS

Discover the possibile operations for any route:

```shell
$ curl --insecure \
    -H 'Accept: application/json; indent=4' \
    -X OPTIONS https://0.0.0.0:8000/reports/
```

#### POST

Change the `title` and `description`, if you'd like:

```shell
$ curl --insecure \
    -d '{"title": "New Report", "description": "Description"}' \
    -H "Content-Type: application/json" \
    -X POST https://0.0.0.0:8000/reports/
```

#### GET List

```shell
$ curl --insecure \
    -H 'Accept: application/json; indent=4' \
    https://127.0.0.1:8000/reports/
```

#### GET Detail

Replace the UUID with an existing resource's UUID:

```shell
$ curl --insecure \
    -H 'Accept: application/json; indent=4' \
    https://127.0.0.1:8000/reports/923bacc1-56e2-4e1d-84d4-b52f34d35e3a/
```

#### DELETE

Replace the UUID with an existing resource's UUID:

```shell
$ curl --insecure \
    -H 'Accept: application/json; indent=4' \
    -X DELETE https://127.0.0.1:8000/reports/923bacc1-56e2-4e1d-84d4-b52f34d35e3a/
```

## Explanations

### HTTPS Requirement

For the purposes of simple deployment, this app was provided a TLS cert and https URL requirement. As it is not a trusted cert, a web browser or CLI tool will be wary and warn that the cert is not legit. On a deployment, I would use [Let's Encrypt](https://letsencrypt.org/) to handle my certs, worry-free.

### Database

For the purposes of simple deployment, [SQLite3](https://www.sqlite.org/index.html) is used. For a light app without a need for full-text searchable indices, this is sufficient. In [Django](https://www.djangoproject.com/), it's possible to change the database backend to something like [Postgres](https://docs.djangoproject.com/en/2.2/ref/contrib/postgres/) without any application code changes.

### UUIDv4 Unique IDs

The `User` and `Report` models have been assigned UUIDv4 IDs to avoid account/resource discoverability by enumeration.

### Created By / Updated By

If a `User` is logged in with credentials, the resource will be recorded with a foreign key relationship.

### URLs Instead of IDs

For speedy iteration and fun testing, URLs are included in each resource instead of ID fields.
