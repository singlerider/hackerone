from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid


# Create your models here.
class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class Report(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=100, null=False)
    description = models.TextField(max_length=1024, blank=True)
    created_by = models.ForeignKey(
        User, related_name="repot_created_by_user",
        null=True, on_delete=models.CASCADE
    )
    updated_by = models.ForeignKey(
        User, related_name="report_updated_by_user",
        null=True, on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return (
            f"{self.title}, created by {self.created_by} at {self.created_at}")

    class Meta:
        indexes = [
            models.Index(fields=["title"]),
            models.Index(fields=["created_by"]),
            models.Index(fields=["created_at"]),
        ]
        get_latest_by = "created_at"
