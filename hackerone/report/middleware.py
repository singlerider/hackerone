from report.models import Report


def add_created_by_to_validated_data(self, validated_data):
    if self.acting_user().is_anonymous:
        validated_data["created_by"] = None
        validated_data["updated_by"] = None
    else:
        validated_data["created_by"] = self.acting_user()
        validated_data["updated_by"] = self.acting_user()
    return validated_data


def add_updated_by_to_instance(self, instance):
    if self.acting_user().is_anonymous:
        instance.updated_by = None
    else:
        instance.updated_by = self.acting_user()
    return instance


class CreateUpdateMiddleware(object):

    def __init__(self, request):
        self.request = request

    def acting_user(self):
        if hasattr(self.request.user, "_wrapped"):
            return self.request.user._wrapped
        return self.request.user


class ReportCreateUpdateMiddleware(CreateUpdateMiddleware):

    def create(self, validated_data):
        validated_data = add_created_by_to_validated_data(self, validated_data)
        return Report.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.description = validated_data.get(
            "description", instance.description)
        instance = add_updated_by_to_instance(self, instance)
        instance.save()
        return instance
