from django.contrib.auth.models import Group
from rest_framework import serializers
from report.models import Report, User
from report import middleware


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("url", "username", "email", "groups")


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ("url", "name")


class ReportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Report
        fields = (
            "url", "title", "description", "created_by", "updated_by",
            "created_at", "updated_at"
        )


class ReportCreateUpdateSerializer(serializers.HyperlinkedModelSerializer):

    def create(self, validated_data):
        user = middleware.ReportCreateUpdateMiddleware(
            self.context["request"]).create(validated_data)
        user.password = None
        return user

    def update(self, instance, validated_data):
        return middleware.ReportCreateUpdateMiddleware(
            self.context["request"]).update(instance, validated_data)

    class Meta:
        model = Report
        fields = (
            "url", "title", "description"
        )
