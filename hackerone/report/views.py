from django.contrib.auth.models import Group
from report import serializers
from report.models import Report, User
from report.serializers import GroupSerializer
from rest_framework import viewsets
from rest_framework.authentication import (BasicAuthentication,
                                           SessionAuthentication)
from rest_framework.viewsets import ModelViewSet


class UserViewSet(viewsets.ModelViewSet):
    """
    API Endpoint for registering users
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class ReportViewSet(ModelViewSet):
    """
    API endpoint that allows reports to be viewed or edited.
    """
    queryset = Report.objects.all()

    def get_serializer_class(self):
        if self.action == "create":
            return serializers.ReportCreateUpdateSerializer
        if self.action == "update":
            return serializers.ReportCreateUpdateSerializer
        return serializers.ReportSerializer
